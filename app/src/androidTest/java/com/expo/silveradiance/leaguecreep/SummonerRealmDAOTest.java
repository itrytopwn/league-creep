package com.expo.silveradiance.leaguecreep;

import android.support.test.InstrumentationRegistry;
import android.support.test.annotation.UiThreadTest;
import android.support.test.runner.AndroidJUnit4;

import com.expo.silveradiance.leaguecreep.db.RealmLiveData;
import com.expo.silveradiance.leaguecreep.db.dao.SummonerRealmDAO;
import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static org.junit.Assert.*;

/**
 * Created by Max on 04-Mar-18.
 */
@RunWith(AndroidJUnit4.class)
public class SummonerRealmDAOTest {

    Realm realm;

    SummonerRealmDAO summonerRealmDAO;

    @Before
    @UiThreadTest
    public void setup() {
        Realm.init(InstrumentationRegistry.getContext()
                                          .getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().inMemory()
                                                                                .name("test-db")
                                                                                .build();
        if (Objects.nonNull(realm)) {
            Realm.deleteRealm(realmConfiguration);
        }
        realm = Realm.getInstance(realmConfiguration);
        summonerRealmDAO = new SummonerRealmDAO(realm);
    }


    @After
    @UiThreadTest
    public void tearDown() {
        summonerRealmDAO = null;

        realm.executeTransaction(realm -> realm.deleteAll());
        realm.close();
    }


    public SummonerDto getTestSummonerDto() {
        SummonerDto summonerDto = new SummonerDto();
        summonerDto.setAccountId(123456789L);
        summonerDto.setId(123456789L);
        summonerDto.setName("test");
        summonerDto.setProfileIconId(0);
        summonerDto.setRevisionDate(123456789L);
        summonerDto.setSummonerLevel(1L);
        return summonerDto;
    }

    @Test
    @UiThreadTest
    public void store() throws Exception {
        assertNotNull(summonerRealmDAO);
        SummonerDto summonerDto = this.getTestSummonerDto();

        summonerRealmDAO.store(summonerDto);
        assertEquals(1, summonerRealmDAO.getAll()
                                        .stream()
                                        .count());
    }


    @Test
    @UiThreadTest
    public void getById() throws Exception {
        assertNotNull(summonerRealmDAO);
        final SummonerDto summonerDto = this.getTestSummonerDto();

        summonerRealmDAO.store(summonerDto);
        RealmLiveData<SummonerDto> dtoRealmLiveData = summonerRealmDAO.getById(summonerDto.getId());


        dtoRealmLiveData.observeForever(summonerDtos -> {
            SummonerDto dto = summonerDtos.stream()
                                          .findAny()
                                          .get();

            assertNotNull(dto);
            assertEquals(dto.getAccountId(), summonerDto.getAccountId());
        });
    }


}