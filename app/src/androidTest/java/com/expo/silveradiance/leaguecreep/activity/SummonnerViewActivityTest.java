package com.expo.silveradiance.leaguecreep.activity;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.InstrumentationRegistry;
import android.support.test.annotation.UiThreadTest;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.expo.silveradiance.RecyclerViewItemCountAssertion;
import com.expo.silveradiance.leaguecreep.R;
import com.expo.silveradiance.leaguecreep.db.dao.SummonerRealmDAO;


import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.expo.silveradiance.RecyclerViewItemCountAssertion.withItemCount;


@RunWith(AndroidJUnit4.class)
@LargeTest
public class SummonnerViewActivityTest {

    private String mStringToBetyped = "Espresso";

    @Rule
    public ActivityTestRule<SummonerViewActivity> mActivityRule = new ActivityTestRule<>(
            SummonerViewActivity.class,false,true);

    @Before
    public void setup() {
        Realm.init(InstrumentationRegistry.getContext()
                                          .getApplicationContext());
        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder().inMemory()
                                                                                .name("test-db")
                                                                                .build();
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    @Test
    public void changeText_sameActivity() {
        // Type text and then press the button.
        onView(withId(R.id.search_edit_text))
                .perform(typeText(mStringToBetyped));
        onView(withId(R.id.search_confirm_button)).perform(click());
        onView(withId(R.id.summoner_list)).check(withItemCount(5));
        // Check that the text was changed.

    }


}