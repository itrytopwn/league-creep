package com.expo.silveradiance.leaguecreep.activity

import android.arch.lifecycle.LiveData
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.expo.silveradiance.leaguecreep.R
import kotlinx.android.synthetic.main.activity_main.*
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v4.app.ActivityOptionsCompat
import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto
import com.expo.silveradiance.leaguecreep.viewmodel.SummonerViewModel
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.support.v4.util.Pair
import android.widget.Toast
import kotlinx.android.synthetic.main.summoner_row.view.*


/**
 * Created by Max on 14-Oct-17.
 */

class SummonerViewActivity : AppCompatActivity() {

    companion object {
        private val TAG: String = "SummonerViewActivity"
    }

    private var summonerViewModel: SummonerViewModel? = null

    private var liveData: LiveData<SummonerDto>? = null

    private var recyclerView: RecyclerView? = null

    private var adapter: SummonerSearchViewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        summonerViewModel = ViewModelProviders.of(this).get(SummonerViewModel::class.java)
        summonerViewModel!!.subscribeAllSummoners()

        if (summonerViewModel!!.getSummoner() != null) {
            liveData = summonerViewModel!!.getSummoner()
            liveData!!.observe(this, Observer<SummonerDto> { summoner: SummonerDto? ->
                if (summoner!!.isLoaded) {
                    val summoner_data = this.summoner_data
                    val string = summoner.id!!.toString()
                    summoner_data.text = string
                    summoner_data.visibility = View.VISIBLE
                    val text = summoner_data.text
                }
            })
        }

        this.initializeRecyclerView()


    }

    private fun initializeRecyclerView() {
        recyclerView = this.summoner_list

        adapter = SummonerSearchViewAdapter(summonerViewModel!!.getAllSummoners())

        adapter!!.setOnItemClickListener(object : SummonerSearchViewAdapter.ClickListener {
            override fun onItemClick(position: Int, v: View) {
                val summonerId = adapter?.getItem(position)!!.getId()
                Log.d(SummonerViewActivity.TAG, "Starting Summoner Detail Activity ID: $summonerId")
                startSummonerDetailActivity(v, summonerId)
            }

            override fun onItemLongClick(position: Int, v: View) {
                val summonerId = adapter?.getItem(position)?.getId()
                Toast.makeText(applicationContext, summonerId.toString(), Toast.LENGTH_SHORT).show()
            }
        })
        recyclerView?.layoutManager = LinearLayoutManager(this)
        recyclerView?.setAdapter(adapter)
        recyclerView?.setHasFixedSize(true)

    }

    fun startSummonerDetailActivity(view: View, summonerId: Long) {
        val intent: Intent = Intent(this, SummonerDetailActivity::class.java)

        val putExtra = intent.putExtra(SummonerDetailActivity.SUMMONER_ID, summonerId)
        val imagePair: Pair<View, String> = Pair(view.summoner_row_image, getString(R.string.summoner_detail_transition_icon))
        val namePair: Pair<View, String> = Pair(view.summoner_row_name, getString(R.string.summoner_detail_transition_name))

        val optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imagePair, namePair)

        startActivity(intent, optionsCompat.toBundle())
    }

    fun searchClicked(v: View) {
        val searchText = this.search_edit_text!!.text.toString()

        if (searchText.isNotEmpty()) {
            if (liveData != null) {
                liveData!!.removeObservers(this)
            }

            liveData = summonerViewModel!!.subscribeOnSummonerByName(searchText)

            liveData!!.observe(this, Observer<SummonerDto> { summoner: SummonerDto? ->
                if (summoner != null) {
                    val summoner_data = this.summoner_data
                    val string = summoner.toString()
                    summoner_data.text = string
                    summoner_data.visibility = View.VISIBLE
                    val text = summoner_data.text
                }
            })
        }
    }
}
