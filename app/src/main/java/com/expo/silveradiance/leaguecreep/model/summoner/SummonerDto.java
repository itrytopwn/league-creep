
package com.expo.silveradiance.leaguecreep.model.summoner;

import com.expo.silveradiance.leaguecreep.model.match.Match;
import com.google.gson.annotations.SerializedName;
import com.google.gson.annotations.Expose;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class SummonerDto extends RealmObject {

    @SerializedName("profileIconId")
    @Expose
    @PrimaryKey
    public Integer profileIconId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("summonerLevel")
    @Expose
    public long summonerLevel;

    @SerializedName("accountId")
    @Expose
    public long accountId;

    @SerializedName("id")
    @Expose
    public long id;

    @SerializedName("revisionDate")
    @Expose
    public long revisionDate;

    public RealmList<Match> matchHistory;

    private final static long serialVersionUID = 2675822951107104169L;

    public Integer getProfileIconId() {
        return profileIconId;
    }

    public void setProfileIconId(Integer profileIconId) {
        this.profileIconId = profileIconId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getSummonerLevel() {
        return summonerLevel;
    }

    public void setSummonerLevel(Long summonerLevel) {
        this.summonerLevel = summonerLevel;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRevisionDate() {
        return revisionDate;
    }

    public void setRevisionDate(Long revisionDate) {
        this.revisionDate = revisionDate;
    }
}