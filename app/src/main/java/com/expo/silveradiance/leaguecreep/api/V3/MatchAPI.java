package com.expo.silveradiance.leaguecreep.api.V3;

import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Max on 14-Oct-17.
 */

public interface MatchAPI {

        @GET("/lol/match/v3/matchlists/by-account/{accountId}/recent")
        Observable<SummonerDto> getRecentMatches(@Path("accountId") Long accountId);


}
