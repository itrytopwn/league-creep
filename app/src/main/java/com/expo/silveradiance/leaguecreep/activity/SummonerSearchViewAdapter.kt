package com.expo.silveradiance.leaguecreep.activity

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.expo.silveradiance.leaguecreep.R
import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto
import com.squareup.picasso.Picasso
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter


/**
 * Created by Max on 19-Oct-17.
 */


class SummonerSearchViewAdapter(data: OrderedRealmCollection<SummonerDto>) : RealmRecyclerViewAdapter<SummonerDto, SummonerSearchViewAdapter.SummonerViewHolder>(data, true) {
    var picasso: Picasso? = null

    companion object {
        var clickListener: ClickListener? = null
    }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SummonerViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.summoner_row, parent, false)
        picasso = Picasso.Builder(parent.context).build()

        return SummonerViewHolder(itemView)
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: SummonerViewHolder, position: Int) {
        val obj = getItem(position)
        holder.data = obj
        holder.summoner_card_id.text = obj!!.id.toString()
        holder.summoner_card_name.text = obj.name.toString()

        picasso?.load("http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/${holder.data!!.profileIconId}.png")!!.into(holder.summoner_image)
    }


    override fun getItemId(index: Int): Long {

        return getItem(index)!!.id
    }


    inner class SummonerViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener {
        var summoner_card_name: TextView
        var summoner_card_id: TextView
        var summoner_image: ImageView
        var data: SummonerDto? = null

        init {
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            summoner_card_name = view.findViewById<View>(R.id.summoner_row_name) as TextView
            summoner_card_id = view.findViewById<View>(R.id.summoner_row_text) as TextView
            summoner_image = view.findViewById<View>(R.id.summoner_row_image) as ImageView
        }


        override fun onClick(v: View) {
            clickListener?.onItemClick(adapterPosition, v)
        }

        override fun onLongClick(v: View): Boolean {
            clickListener?.onItemLongClick(adapterPosition, v)
            return false
        }

    }

    fun setOnItemClickListener(clickListener: ClickListener) {
        SummonerSearchViewAdapter.clickListener = clickListener
    }

    public interface ClickListener {
        fun onItemClick(position: Int, v: View)
        fun onItemLongClick(position: Int, v: View)
    }
}