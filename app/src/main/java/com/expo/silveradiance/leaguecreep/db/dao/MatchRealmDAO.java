package com.expo.silveradiance.leaguecreep.db.dao;

import com.expo.silveradiance.leaguecreep.db.RealmLiveData;
import com.expo.silveradiance.leaguecreep.model.match.Match;
import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.expo.silveradiance.leaguecreep.utility.RealmUtils.asLiveData;

public class MatchRealmDAO {

    private Realm realm;

    public MatchRealmDAO(Realm realm) {
        if (realm == null) throw new IllegalArgumentException();
        this.realm = realm;
    }

    public void store(Match match) {
        realm.executeTransaction(realm -> {
            realm.copyToRealmOrUpdate(match);
        });
    }

    public RealmLiveData<Match> getById(Long id) {
        return asLiveData(
                realm.where(Match.class)
                     .equalTo("id", id)
                     .findAllAsync()
        );
    }

    public RealmResults<Match> getAll() {
        return realm.where(Match.class)
                    .sort("id")
                    .findAllAsync();

    }


}
