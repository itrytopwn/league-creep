@file:JvmName("RealmUtils")
package com.expo.silveradiance.leaguecreep.utility
// pretty name for utils class if called from


import com.expo.silveradiance.leaguecreep.db.RealmLiveData
import com.expo.silveradiance.leaguecreep.db.dao.SummonerRealmDAO
import io.realm.Realm
import io.realm.RealmModel
import io.realm.RealmResults

fun Realm.summonerDAO(): SummonerRealmDAO = SummonerRealmDAO(this)


fun <T:RealmModel> RealmResults<T>.asLiveData() = RealmLiveData<T>(this)