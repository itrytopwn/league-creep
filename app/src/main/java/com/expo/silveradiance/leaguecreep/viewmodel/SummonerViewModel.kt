package com.expo.silveradiance.leaguecreep.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Transformations
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.expo.silveradiance.leaguecreep.api.V3.SummonerAPI
import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto
import com.expo.silveradiance.leaguecreep.db.dao.SummonerRealmDAO
import com.expo.silveradiance.leaguecreep.utility.Const
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import io.reactivex.observers.DisposableObserver


/**
 * Created by Max on 14-Oct-17.
 */
class SummonerViewModel : ViewModel() {

    private var realm: Realm? = null

    private var summonerDB: SummonerRealmDAO? = null;

    private var summonerAPI: SummonerAPI? = null

    private var summoner: LiveData<SummonerDto>? = null;

    private var allSummoners: RealmResults<SummonerDto>? = null;

    init {
        // TODO Change way APIs are accessible
        summonerAPI = initializeAPI()
        realm = initializeDB();
        summonerDB = SummonerRealmDAO(realm!!)

        val dto = SummonerDto()
        dto.accountId = 231254L
        dto.id = 88005553535L
        dto.name = "Shistormo"

        summonerDB!!.store(dto)
    }


    private fun initializeAPI(): SummonerAPI {
        val gson = GsonBuilder()
                .setLenient()
                .create()

        val defaultHttpClient = OkHttpClient.Builder()
                .addInterceptor({ chain ->
                    val request = chain.request()
                            .newBuilder()
                            .addHeader("X-Riot-Token", Const.RIOT_API_KEY)
                            .build()
                    chain.proceed(request)
                })
                .build()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://euw1.api.riotgames.com")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(defaultHttpClient)
                .build()


        return retrofit.create(SummonerAPI::class.java)
    }

    private fun initializeDB(): Realm {
        // Get a Realm instance for this thread
        return Realm.getDefaultInstance();
    }

    public fun subscribeOnSummonerByName(text: String?): LiveData<SummonerDto>? {
        val liveData = summonerDB!!.getByName(text)

        return Transformations.map(liveData, { res: RealmResults<SummonerDto> ->
            if (res.size != 0 && res.isLoaded) {
                res.get(0)
            } else {
                summonerAPI!!.fetchSummonerByName(text)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(this.saveSummoner())


                null
            }
        })
    }

    public fun subscribeOnSummonerById(id: Long): LiveData<SummonerDto>? {
        val liveData = summonerDB!!.getById(id)

        return Transformations.map(liveData, { res: RealmResults<SummonerDto> ->
            if (res.size != 0 && res.isLoaded) {
                res.get(0)
            } else {
                summonerAPI!!.fetchSummonerById(id)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(this.saveSummoner())


                null
            }
        })
    }



    private fun saveSummoner(): DisposableObserver<SummonerDto> {
        return object : DisposableObserver<SummonerDto>() {
            override fun onNext(summoner: SummonerDto) {
                //Handle logic
                summonerDB!!.store(summoner)
            }

            override fun onError(e: Throwable) {
                //Handle error
                Log.e("SummonerViewModel", "Can't find summoner for", e)
            }

            override fun onComplete() {

            }
        };
    }


    public fun getSummoner(): LiveData<SummonerDto>? {
        return summoner;
    }

    public fun subscribeAllSummoners() {
        allSummoners = summonerDB!!.all
    }

    public fun getAllSummoners(): RealmResults<SummonerDto> {
        return this.allSummoners!!
    }

//    public fun getRecentMatches(summoner: SummonerDto): RealmResults<SummonerDto> {
//
//    }

    /**
     * This method will be called when this ViewModel is no longer used and will be destroyed.
     *
     *
     * It is useful when ViewModel observes some data and you need to clear this subscription to
     * prevent a leak of this ViewModel... Like the Realm instance!
     */
    override fun onCleared() {
        realm!!.close()
        super.onCleared()
    }


}