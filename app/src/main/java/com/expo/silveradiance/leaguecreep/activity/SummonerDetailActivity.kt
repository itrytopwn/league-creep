package com.expo.silveradiance.leaguecreep.activity

import android.app.Activity
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Build
import android.os.Bundle
import android.support.transition.TransitionInflater
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import com.expo.silveradiance.leaguecreep.R
import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto
import com.expo.silveradiance.leaguecreep.viewmodel.SummonerViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.summoner_details.*
import android.view.ViewTreeObserver
import android.widget.ImageView
import com.squareup.picasso.Callback
import kotlinx.android.synthetic.main.summoner_row.*


/**
 * Created by Max on 14-Oct-17.
 */

class SummonerDetailActivity : AppCompatActivity() {
    companion object {

        private val TAG: String = "SummonerDetailActivity"
        public val SUMMONER_ID: String = "SUMMONER_ID"
    }

    var picasso: Picasso? = null

    private var summonerViewModel: SummonerViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        // Postpone the shared element enter transition.
        postponeEnterTransition();
//        TransitionInflater
//                .from(this)
//                .inflateTransition(
//                        android.R.transition.move // you can change this
//                )

        super.onCreate(savedInstanceState)
        picasso = Picasso.with(this)

        setContentView(R.layout.summoner_details)
        summonerViewModel = ViewModelProviders.of(this)
                .get(SummonerViewModel::class.java)

        val passedSummonerId: Long = this.intent.getLongExtra(SummonerDetailActivity.SUMMONER_ID, -1);
        summonerViewModel!!.subscribeOnSummonerById(passedSummonerId)!!.observe(this, Observer<SummonerDto> { summoner: SummonerDto? ->
            if (summoner != null) {
                this.details_summoner_name.text = summoner.getName()
                picasso?.load("http://ddragon.leagueoflegends.com/cdn/6.24.1/img/profileicon/${summoner.profileIconId}.png")!!
                        .resize(400, 400)
                        .into(this.details_summoner_image)
                scheduleStartPostponedTransition(this.details_summoner_name)
            }
        })


    }

    /**
     * Schedules the shared element transition to be started immediately
     * after the shared element has been measured and laid out within the
     * activity's view hierarchy. Some common places where it might make
     * sense to call this method are:
     *
     * (1) Inside a Fragment's onCreateView() method (if the shared element
     * lives inside a Fragment hosted by the called Activity).
     *
     * (2) Inside a Picasso Callback object (if you need to wait for Picasso to
     * asynchronously load/scale a bitmap before the transition can begin).
     *
     * (3) Inside a LoaderCallback's onLoadFinished() method (if the shared
     * element depends on data queried by a Loader).
     */
    private fun scheduleStartPostponedTransition(sharedElement: View) {
        sharedElement.viewTreeObserver.addOnPreDrawListener(
                object : ViewTreeObserver.OnPreDrawListener {
                    override fun onPreDraw(): Boolean {
                        sharedElement.viewTreeObserver.removeOnPreDrawListener(this)
                        startPostponedEnterTransition()
                        return true
                    }
                })
    }


}
