package com.expo.silveradiance.leaguecreep.model.static_data;


import java.io.Serializable;
import java.util.Map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RealmDto implements Serializable {

    @SerializedName("lg")
    @Expose
    public String lg;
    @SerializedName("dd")
    @Expose
    public String dd;
    @SerializedName("l")
    @Expose
    public String l;
    @SerializedName("n")
    @Expose
    public Map<String,String> n;
    @SerializedName("profileiconmax")
    @Expose
    public Integer profileiconmax;
    @SerializedName("v")
    @Expose
    public String v;
    @SerializedName("cdn")
    @Expose
    public String cdn;
    @SerializedName("css")
    @Expose
    public String css;
    private final static long serialVersionUID = 3143854207322925943L;

}