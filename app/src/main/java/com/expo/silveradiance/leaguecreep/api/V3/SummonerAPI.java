package com.expo.silveradiance.leaguecreep.api.V3;

import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Max on 14-Oct-17.
 */

public interface SummonerAPI {

        @GET("/lol/summoner/v3/summoners/by-name/{accountId}")
        Observable<SummonerDto> fetchSummonerById(@Path("accountId") Long accountId);

        @GET("/lol/summoner/v3/summoners/by-name/{summonerName}")
        Observable<SummonerDto> fetchSummonerByName(@Path("summonerName") String summonerName);

        @GET("/lol/summoner/v3/summoners/{summonerId}")
        Observable<SummonerDto> getSummoner(@Path("summonerId") Long accountId);


}
