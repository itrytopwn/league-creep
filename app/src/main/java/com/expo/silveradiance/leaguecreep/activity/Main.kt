package com.expo.silveradiance.leaguecreep.activity

import android.app.Application

import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by Max on 14-Oct-17.
 */


class Main : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this);
        val configuration = RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(configuration);
    }
}


