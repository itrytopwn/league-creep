package com.expo.silveradiance.leaguecreep.db.dao;

import com.expo.silveradiance.leaguecreep.model.summoner.SummonerDto;
import com.expo.silveradiance.leaguecreep.db.RealmLiveData;

import io.realm.Realm;
import io.realm.RealmResults;

import static com.expo.silveradiance.leaguecreep.utility.RealmUtils.asLiveData;

/**
 * Created by Max on 14-Oct-17.
 */

public class SummonerRealmDAO {
    private Realm realm;

    public SummonerRealmDAO(Realm realm) {
        if(realm == null) throw new IllegalArgumentException();
        this.realm = realm;
    }

    public void store(SummonerDto summoner) {

        realm.executeTransaction(realm -> {
            realm.copyToRealmOrUpdate(summoner);
        });
    }

    public RealmLiveData<SummonerDto> getById(Long id) {
        return asLiveData(
                realm.where(SummonerDto.class)
                     .equalTo("id", id)
                     .findAllAsync()
        );
    }

    public RealmResults<SummonerDto> getAll() {
        return realm.where(SummonerDto.class).sort("id").findAllAsync();

    }

    public RealmLiveData<SummonerDto> getByName(String name) {
        return asLiveData(
                realm.where(SummonerDto.class)
                     .equalTo("name", name)
                     .findAllAsync()
        );
    }

}
